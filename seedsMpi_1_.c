#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#define TER 300
//
void seed_1(int xstation,int ystation,int *ss)
{
	int t=0;
while(t<TER)
	{	
	for(int ii=0;ii<xstation;ii++)
	{
		for(int jj=0;jj<ystation;jj++)//
		{		
			int x=ii-1;
			int y=jj-1;
			if(x<0){
				x=0;
			}
			if(y<0){
				y=0;
			}
			int count =ss[(x)*ystation+y]+ss[(x)*ystation+jj]+ss[(x)*ystation+jj+1]
			+ss[ii*ystation+y] +ss[ii*ystation+jj+1]
			+ss[(ii+1)*ystation+y]+ss[(ii+1)*ystation+jj]+ss[(ii+1)*ystation+jj+1];
			if(count ==2)//born
				ss[ii*ystation+jj]=1;
			else //off
				ss[ii*ystation+jj]=0;
		}
		}
		
		t++;
	}
	
}		

int main(int argc, char** argv)
{
	int xstation,ystation;
	if(argc>1){
		xstation=atoi(argv[1]);
	}
	if(argc>2){
		ystation=atoi(argv[2]);
	
	}
	int *ss=(int*)malloc(2*xstation*2*ystation*sizeof(int));
	int *ss1=(int*)malloc(2*xstation*2*ystation*sizeof(int));
	 MPI_Init(&argc, &argv);
    int p, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int NPROWS=sqrt(p);  /* number of rows in _decomposition_ */
    int NPCOLS=sqrt(p);  /* number of cols in _decomposition_ */
    int BLOCKROWS = xstation/NPROWS;  /* number of rows in _block_ */
    int BLOCKCOLS = ystation/NPCOLS; /* number of cols in _block_ */
	
    if (p != NPROWS*NPCOLS) {
        fprintf(stderr,"Error: number of PEs %d != %d x %d\n", p, NPROWS, NPCOLS);
        MPI_Finalize();
        exit(-1);
    }
	if(rank==0){
	for(int i=0;i<=xstation;i++)
	{
		for(int j=0;j<=ystation;j++)
		{
			ss[i*ystation+j]=rand()%2; 
		}
	}
	}
	
	MPI_Datatype blocktype;
    MPI_Datatype blocktype2;
	MPI_Type_vector(BLOCKROWS, BLOCKCOLS,ystation, MPI_INT, &blocktype2);
    MPI_Type_create_resized( blocktype2, 0, sizeof(int), &blocktype);
    MPI_Type_commit(&blocktype);
	int *disps=(int*)malloc(2*xstation*2*ystation*sizeof(int));
    int *counts=(int*)malloc(2*xstation*2*ystation*sizeof(int));
	 for (int ii=0; ii<xstation; ii++) {
        for (int jj=0; jj<ystation; jj++) {
            disps[ii*NPCOLS+jj] = ii*ystation*BLOCKROWS+jj*BLOCKCOLS;
            counts [ii*NPCOLS+jj] = 1;
        }
    }

    MPI_Scatterv(ss, counts, disps, blocktype, ss1, BLOCKROWS*BLOCKCOLS, MPI_INT, 0, MPI_COMM_WORLD);
	
	seed_1(BLOCKROWS,BLOCKCOLS,ss1);
	
	MPI_Gather(ss1,BLOCKROWS*BLOCKCOLS,MPI_INT,ss,BLOCKROWS*BLOCKCOLS,MPI_INT,0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	if(rank==0){
		
	for(int i=0;i<xstation;i++)
	{
		for(int j=0;j<ystation;j++)
		{
			printf("%3d ",ss[i*ystation+j]);
		}
		printf("\n");
	
	}
	}
	free(ss);
	free(disps);
	free(counts);
	free(ss1);
	//
	return 0;
}
